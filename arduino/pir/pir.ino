// https://www.modmypi.com/blog/raspberry-pi-gpio-sensing-motion-detection
// https://learn.adafruit.com/pir-passive-infrared-proximity-motion-sensor/using-a-pir-w-arduino

int LED_PIN = 6;
int PIR_PIN = 7;
int pir_val = 0;
unsigned long pir_counter = 0;
int mdelay = 1000;
unsigned long cmil = 0;
unsigned long omil = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(LED_PIN, OUTPUT);
  pinMode(PIR_PIN, INPUT);
  Serial.begin(9600);
  omil = millis();
}

void loop() {
  // put your main code here, to run repeatedly:
  pir_val = digitalRead(PIR_PIN);
  //Serial.println(pir_val);
  if (pir_val == HIGH) {
    digitalWrite(LED_PIN, HIGH);
  } else {
    digitalWrite(LED_PIN, LOW);
  }

  cmil = millis();
  if (cmil - omil > mdelay) {
    omil = cmil;
    pir_counter += pir_val;
    Serial.println(pir_counter);
  }
}
