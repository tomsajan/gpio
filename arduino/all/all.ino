//https://playground.arduino.cc/Learning/PhotoResistor 

// photoresistor
int LDR_PIN = A0;
int LDR_TRIGGER_PIN = 3;
int LDR_VAL = 0;
int LED_PIN = LED_BUILTIN;


// sound
const byte SOUND_INTERRUPT_PIN = 2;   // digital out of the mic
unsigned int ana = 0;
unsigned long sound_count = 0;
volatile unsigned long interrupt_sound_count = 0;

// pir
int PIR_PIN = 4;
int pir_val = 0;
unsigned long pir_counter = 0;

// general
unsigned long previous_millis = 0;
unsigned long current_millis = 0;
const long sample_interval =  1000; // count every second

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  // photoresistor
  pinMode(LED_PIN, OUTPUT);
  pinMode(LDR_TRIGGER_PIN, OUTPUT);
  digitalWrite(LDR_TRIGGER_PIN, LOW);

  // sound
  pinMode(SOUND_INTERRUPT_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(SOUND_INTERRUPT_PIN), sound_detect, RISING);


  //pir
  pinMode(PIR_PIN, INPUT);

  // general
  digitalWrite(LED_PIN, LOW);
}

void measure_light(){
  digitalWrite(LDR_TRIGGER_PIN, HIGH);
  LDR_VAL = analogRead(LDR_PIN);
  digitalWrite(LDR_TRIGGER_PIN, LOW);
}

void measure_sound(){
  if (interrupt_sound_count > 0){
      sound_count++;
      interrupt_sound_count = 0;
    }
}

void measure_pir(){
    pir_val = digitalRead(PIR_PIN);
}


void sound_detect() {
  interrupt_sound_count++;
}

void loop() {
  // put your main code here, to run repeatedly:
//  digitalWrite(LDR_TRIGGER_PIN, HIGH);
//  LDR_VAL = analogRead(LDR_PIN);
//  digitalWrite(LDR_TRIGGER_PIN, LOW);
  //Serial.println(1023-LDR_VAL);
//  Serial.println(LDR_VAL);
  //analogWrite(LED_PIN, LDR_VAL/2);
  //delay(100);

  
  pir_val = digitalRead(PIR_PIN);
  if (pir_val == HIGH) {
    digitalWrite(LED_PIN, HIGH);
  } else {
    digitalWrite(LED_PIN, LOW);
  }

  current_millis = millis();
  if (current_millis - previous_millis > sample_interval){
    noInterrupts();
    
    measure_sound();
    measure_light();
    pir_counter += pir_val;
    
    interrupts();
    previous_millis = current_millis;
    Serial.println(sound_count);
    Serial.println(LDR_VAL);
    Serial.println(pir_counter);
  }
  
  

}


