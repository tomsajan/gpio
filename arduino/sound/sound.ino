//https://tkkrlab.nl/wiki/Arduino_KY-038_Microphone_sound_sensor_module

//int S_PIN_A = A0;
//int S_PIN_D = 7;
//int LED_PIN = 13;
const byte SOUND_INTERRUPT_PIN = 2; //digital out of the mic

int dig = 0;
unsigned int ana = 0;

unsigned long previous_millis = 0;
unsigned long current_millis = 0;
const long sound_interval =  1000; // count every second
unsigned long sound_count = 0;

volatile unsigned long interrupt_sound_count = 0;

void setup() {
  // put your setup code here, to run once:
  //pinMode(LED_PIN, OUTPUT);
  //pinMode(S_PIN_D, INPUT);
  pinMode(SOUND_INTERRUPT_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(SOUND_INTERRUPT_PIN), sound_detect, RISING);
  Serial.begin(9600);
  
}

void sound_detect() {
  interrupt_sound_count++;

}

void loop() {
  // put your main code here, to run repeatedly:
  //ana = analogRead(S_PIN_A);
  //dig = digitalRead(S_PIN_D);
  //if (dig == HIGH) digi_count++;
  //digitalWrite(LED_PIN, dig == HIGH ? HIGH : LOW);
  //Serial.println(ana);
  current_millis = millis();
  if (current_millis - previous_millis > sound_interval){
    if (interrupt_sound_count > 0){
      sound_count++;
      interrupt_sound_count = 0;
    }
    previous_millis = current_millis;
  }
  
  Serial.println(sound_count);
}
