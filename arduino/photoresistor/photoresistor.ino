//https://playground.arduino.cc/Learning/PhotoResistor 

int LDR_PIN = A0;
int LDR_TRIGGER_PIN = 2;
int LDR_VAL = 0;
int LED_PIN = LED_BUILTIN;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(LED_PIN, OUTPUT);
  pinMode(LDR_TRIGGER_PIN, OUTPUT);
  digitalWrite(LDR_TRIGGER_PIN, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(LDR_TRIGGER_PIN, HIGH);
  LDR_VAL = analogRead(LDR_PIN);
  digitalWrite(LDR_TRIGGER_PIN, LOW);
  //Serial.println(1023-LDR_VAL);
  Serial.println(LDR_VAL);
  analogWrite(LED_PIN, LDR_VAL/2);
  delay(100);
  
//  if (Serial.available() > 0) {
//    String text = Serial.readString();
//    Serial.print("I got: ");
//    Serial.println(text);
//    
//  }
}
