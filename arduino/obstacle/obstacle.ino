// https://www.nedro.nl/en/various-computer-accessories/8165884-al798-nedro-ir-obstacle-avoidance-sensor-for-arduino-smart-car-3-wire-al798.html?gclid=CjwKCAiA6K_QBRA8EiwASvtjZYGk3QSPxHE5sAtpUB4NZK_9TD_xUQITh5mHmZdEt74HkdN12aOo8BoC7BgQAvD_BwE
// https://codetipste.blogspot.cz/2017/04/ir-sensor-arduino.html

int OBS_PIN = 7;
int LED_PIN = 13;
int val = 0;
void setup() {
  // put your setup code here, to run once:
  pinMode(LED_PIN, OUTPUT);
  pinMode(OBS_PIN, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  val = digitalRead(OBS_PIN);
  digitalWrite(LED_PIN, val);
  Serial.println(val);
  delay(300);
}
