//https://playground.arduino.cc/Learning/PhotoResistor 

// photoresistor
int LDR_PIN = A0;
int LDR_TRIGGER_PIN = 3;
int LDR_VAL = 0;


// sound
const byte SOUND_INTERRUPT_PIN = 2;   // digital out of the mic
unsigned int ana = 0;
unsigned long sound_count = 0;
volatile unsigned long interrupt_sound_count = 0;

// pir
int PIR_PIN = 4;
int pir_val = 0;
unsigned long pir_count = 0;

// general
unsigned long previous_millis = 0;
unsigned long current_millis = 0;
const long sample_interval =  1000; // count every second
int LED_PIN = LED_BUILTIN;

String STATUS = "START";
String command;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  // photoresistor
  pinMode(LED_PIN, OUTPUT);
  pinMode(LDR_TRIGGER_PIN, OUTPUT);
  digitalWrite(LDR_TRIGGER_PIN, LOW);

  // sound
  pinMode(SOUND_INTERRUPT_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(SOUND_INTERRUPT_PIN), sound_detect, RISING);


  //pir
  pinMode(PIR_PIN, INPUT);

  // general
  digitalWrite(LED_PIN, LOW);
  STATUS = "MEASUREMENT";
  
  Serial.println("READY");
}

void measure_light(){
  digitalWrite(LDR_TRIGGER_PIN, HIGH); // turn on voltage 
  LDR_VAL = analogRead(LDR_PIN);       // read value
  digitalWrite(LDR_TRIGGER_PIN, LOW);  // turn off voltage
}

void measure_sound(){
  if (interrupt_sound_count > 0){
      sound_count++;
      interrupt_sound_count = 0;
    }
}

void measure_pir(){
    pir_val = digitalRead(PIR_PIN);
    pir_count += pir_val;
}


// called when interrupt trigerred
void sound_detect() {
  interrupt_sound_count++;
}

void send_data(){
      Serial.print("SOUND:");
      Serial.print(sound_count);
      Serial.print(";LIGHT:");
      Serial.print(LDR_VAL);
      Serial.print(";PIR:");
      Serial.print(pir_count);
      Serial.print(";CRC:");
      Serial.println(sound_count + LDR_VAL + pir_count);

      Serial.flush();

}

void reset() {
  sound_count = 0;
  LDR_VAL = 0;
  pir_count = 0;
}

void communication(){
  if (Serial.available() > 0) {
     command = Serial.readStringUntil('\n');
  } else {
    return;   
  }
  if (command == "GET_DATA"){
    STATUS = "SENDING_DATA";
    send_data();
    STATUS = "DATA_SENT";
  } else if (command == "RESET") {
    reset();
    STATUS = "MEASUREMENT";
  } else if (command == "CONTINUE") {
    STATUS = "MEASUREMENT";
  }
}

void loop() {
  communication();
  current_millis = millis();
  if (STATUS == "MEASUREMENT" && (current_millis - previous_millis > sample_interval)){
    noInterrupts();
    
    measure_sound();
    measure_light();
    measure_pir();

    interrupts();
    
    previous_millis = current_millis;
  }
}


