#! /usr/bin/env python
# http://www.uugear.com/portfolio/dht11-humidity-temperature-sensor-module/

import time
import RPi.GPIO as GPIO

DATA_PIN = 18
# VCC_PIN = 15

HIGH_ZERO_FROM = 6
HIGH_ZERO_TO = 15
HIGH_ONE_FROM = 30
HIGH_ONE_TO = 50


def bin2dec(string_num):
    return str(int(string_num, 2))


def setup():
    GPIO.setmode(GPIO.BCM)
    # GPIO.setup(VCC_PIN, GPIO.OUT, initial=GPIO.HIGH)
    # GPIO.output(VCC_PIN, GPIO.HIGH)


def measurement_init():
    GPIO.setup(DATA_PIN, GPIO.OUT)
    GPIO.output(DATA_PIN, GPIO.HIGH)
    time.sleep(0.025)
    GPIO.output(DATA_PIN, GPIO.LOW)
    time.sleep(0.02)

    GPIO.setup(DATA_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)


def take_data():
    GPIO.setup(DATA_PIN, GPIO.OUT)
    GPIO.output(DATA_PIN, GPIO.HIGH)
    time.sleep(0.025)
    GPIO.output(DATA_PIN, GPIO.LOW)
    time.sleep(0.02)

    GPIO.setup(DATA_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    data = []
    for i in range(0, 5000):
        data.append(GPIO.input(DATA_PIN))
    #print(data)
    # with open('/tmp/out.txt', 'w') as f:
    #     i = 0
    #     for d in data:
    #         f.write('{} {}\n'.format(i, d))
    #         i += 1

    count = 0
    BITS = ""
    try:
        while data[count] == 0:
            count += 1
        while data[count] == 1:
            count += 1

        for bit in range(0, 40):
            bit_count = 0

            while data[count] == 0:
                count += 1

            while data[count] == 1:
                bit_count += 1
                count += 1

            if HIGH_ZERO_FROM <= bit_count <= HIGH_ZERO_TO:
                BITS += "0"
            elif HIGH_ONE_FROM <= bit_count <= HIGH_ONE_TO:
                BITS += "1"
                # TODO: else exception

    except:
        print("ERR_RANGE")
        raise

    HID = bin2dec(BITS[0:8])
    HDD = bin2dec(BITS[8:16])
    TID = bin2dec(BITS[16:24])
    TDD = bin2dec(BITS[24:32])
    CRC = bin2dec(BITS[32:40])

    print(HID, HDD)
    print(TID, TDD)
    print(CRC)
    print(int(HID)+int(HDD)+int(TID)+int(TDD)-int(CRC))

    # if int(Humidity) + int(Temperature) - int(bin2dec(crc)) == 0:
    #     print("Humidity:"+ Humidity +"%")
    #     print("Temperature:"+ Temperature +"C")
    # else:
    #     print("ERR_CRC")


def run():
    #measurement_init()
    take_data()


def cleanup():
    GPIO.cleanup()


if __name__ == '__main__':
    setup()
    try:
        run()
    except KeyboardInterrupt:
        pass
    finally:
        cleanup()
