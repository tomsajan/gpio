#! /usr/bin/env python
# http://www.uugear.com/portfolio/using-light-sensor-module-with-raspberry-pi/
# rpi light module

import time
import RPi.GPIO as GPIO

DATA_PIN = 4

def setup():
    GPIO.setmode(GPIO.BCM)
    #GPIO.setup(17, GPIO.OUT, initial=GPIO.LOW)
    GPIO.setup(DATA_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    #GPIO.add_event_detect(17, GPIO.RISING, callback=my_callback, bouncetime=150)


def run():
    #for i in range(30):
    while True:
        time.sleep(0.5)
        print(GPIO.input(DATA_PIN))


def cleanup():
    GPIO.cleanup()


if __name__ == '__main__':
    setup()
    try:
        run()
    except KeyboardInterrupt:
        cleanup()
