#! /usr/bin/env python

import time
import RPi.GPIO as GPIO


def setup():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(17, GPIO.OUT, initial=GPIO.LOW)


def run():
    for i in range(10):
        time.sleep(0.5)
        GPIO.output(17, GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output(17, GPIO.LOW)


def cleanup():
    GPIO.cleanup()


if __name__ == '__main__':
    setup()
    run()
    cleanup()


