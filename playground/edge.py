#! /usr/bin/env python

import time
import RPi.GPIO as GPIO


def my_callback(channel):
    print("Rising channel {}".format(channel))


def setup():
    GPIO.setmode(GPIO.BCM)
    #GPIO.setup(17, GPIO.OUT, initial=GPIO.LOW)
    GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.add_event_detect(17, GPIO.RISING, callback=my_callback, bouncetime=150)


def run():
    for i in range(30):
        time.sleep(1)
        print(i, 's')


def cleanup():
    GPIO.cleanup()


if __name__ == '__main__':
    setup()
    run()
    cleanup()
