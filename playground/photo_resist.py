#! /usr/bin/env python
# http://www.raspberrypi-spy.co.uk/2012/08/reading-analogue-sensors-with-one-gpio-pin/#prettyPhoto
# "analogue: reading of photoresistor using a capacitor (measuring the time it takes to charge)


import time
import RPi.GPIO as GPIO
import time

DATA_PIN = 17
# VCC_PIN = 27
DISCHARGE_TIME = 1  # in seconds


def setup():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(DATA_PIN, GPIO.OUT, initial=GPIO.LOW)
    # GPIO.setup(VCC_PIN, GPIO.OUT, initial=GPIO.HIGH)
    # GPIO.output(VCC_PIN, GPIO.HIGH)


def measurement_init():
    GPIO.setup(DATA_PIN, GPIO.OUT)
    GPIO.output(DATA_PIN, GPIO.LOW)
    time.sleep(DISCHARGE_TIME)


def take_data():
    GPIO.setup(DATA_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

    val = 0
    while GPIO.input(DATA_PIN) == GPIO.LOW:
        val += 1
    print(val)


def run():
    measurement_init()
    take_data()


def cleanup():
    GPIO.cleanup()


if __name__ == '__main__':
    setup()
    try:
        for _ in range(100):
            run()
    except KeyboardInterrupt:
        pass
    finally:
        cleanup()

