#! /usr/bin/env python

# https://www.modmypi.com/blog/raspberry-pi-gpio-sensing-motion-detection

import time
import RPi.GPIO as GPIO

LED_PIN = 18
PIR_PIN = 4
LED_STATE = False


def motion(pin):
    print("Motion detected")
    GPIO.output(LED_PIN, GPIO.input(PIR_PIN)) # turn on / off the led according to the pir info


# def motion_off(pin):
#     GPIO.output(LED_PIN, GPIO.LOW)


def setup():
    GPIO.setmode(GPIO.BCM)
    # GPIO.setup(17, GPIO.OUT, initial=GPIO.LOW)
    GPIO.setup(PIR_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(LED_PIN, GPIO.OUT, initial=GPIO.LOW)
    # GPIO.add_event_detect(PIR_PIN, GPIO.BOTH, callback=motion, bouncetime=1000)
    GPIO.add_event_detect(PIR_PIN, GPIO.BOTH, callback=motion)
    # GPIO.add_event_detect(PIR_PIN, GPIO.FALLING, callback=motion_off, bouncetime=150)


def run():
    while True:
        time.sleep(100)


def cleanup():
    GPIO.cleanup()


if __name__ == '__main__':
    cleanup()
    setup()
    try:
        run()
    except KeyboardInterrupt:
        pass
    finally:
        cleanup()
